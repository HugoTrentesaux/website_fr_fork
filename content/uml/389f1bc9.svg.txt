
::uml:: format="svg" alt="Diagramme fonctionnement général"

@startuml

server <--> config
config <--> [**keypair**\nFournit un trousseau\ncryptographique au\nserveur par configuration.]
server <--> [**prover**\nProduit des nouveaux\nblocs et realise leur\npreuve de travail.]
[**peersignal**\nEmet periodiquement\nune nouvelle fiche de\npair a jour.] <--> server

package "BMA" {
  [**BMA server**\nServeur HTTP public qui recoit\net transmet les documents\nDuniter au serveur.] <---> server
  [**BMA crawler**\nSollicite les autres noeuds BMA\npour recuperer des documents\nde facon active.] <---> server
  [**BMA router**\nTransmet les documents valides\ntraites par le serveur au reste du\nreseau via BMA.] <---> server
}

package "WS2P" {
    [**WS2P**\nNouvelle couche reseau\nP2P visant a remplacer\nBMA pour la communication\ninter-noeuds.] <--> server
}

@enduml

::end-uml::
