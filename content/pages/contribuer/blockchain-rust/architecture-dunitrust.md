Title: Architecture de Dunitrust
Order: 10
Date: 2018-03-29
Slug: architecture-dunitrust
Authors: elois

Dunitrust est composé de 3 types de crates : 

* Les bibliothèques  
* Les plugins  
* la crate duniter-core

## Arbre des dépendances

![uml]({attach}/uml/d01b3686.png)