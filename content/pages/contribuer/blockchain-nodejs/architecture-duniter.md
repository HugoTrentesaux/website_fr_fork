Title: Architecture
Order: 10
Date: 2017-10-09
Slug: architecture-duniter
Authors: vit

## Architecture réseau des clients

Voici un aperçu de l'architecture de Duniter entre les clients et un serveur.

![uml]({attach}/uml/00182988.svg)
