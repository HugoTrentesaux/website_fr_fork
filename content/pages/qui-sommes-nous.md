Title: Qui sommes-nous ?
Order: 9
Date: 2017-03-27
Slug: qui-sommes-nous
name: about
Authors: cgeek
Lang: fr

Nous sommes de simples citoyens, principalement français, s'efforçant de produire une nouvelle valeur économique appelée « *monnaie libre* », de forme numérique, et respectant les principes établis par la [Théorie Relative de la Monnaie (TRM)](http://trm.creationmonetaire.info/).

Il n'y a pour le moment aucune organisation officielle derrière Duniter, bien que le projet soit toutefois soutenu par la société *Art et Zerty*, EURL détenue par le fondateur du logiciel Duniter Cédric Moreau alias *cgeek*, et possédant la marque Duniter en France.

Si le projet prend de l'ampleur sur le long terme, la création d'une association est déjà envisagée par ses initiateurs. La marque serait alors transmise à cette association.